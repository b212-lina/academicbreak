package com.zuitt.batch212;

import java.util.Scanner;

public class Grades {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double[] grades;
        grades = new double[8];
        double average, sum = 0;

        System.out.println("Enter grade in English:");
        grades[0] = scanner.nextDouble();

        System.out.println("Enter grade in Math:");
        grades[1] = scanner.nextDouble();

        System.out.println("Enter grade in Filipino:");
        grades[2] = scanner.nextDouble();

        System.out.println("Enter grade in Science:");
        grades[3] = scanner.nextDouble();

        System.out.println("Enter grade in History:");
        grades[4] = scanner.nextDouble();

        System.out.println("Enter grade in Music:");
        grades[5] = scanner.nextDouble();

        System.out.println("Enter grade in Arts:");
        grades[6] = scanner.nextDouble();

        System.out.println("Enter grade in Physical Education:");
        grades[7] = scanner.nextDouble();

        for (int i = 0; i < 8; i++) {

            sum = sum + grades[i];

        }

        average = sum / 8;

        System.out.println("The average grade is " + average + ".");

    }
}
