package com.zuitt.batch212;

import java.util.Scanner;

public class AreaCircle {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        // Problem 1: Area of Circle

        System.out.println("Area of Circle Calculator");

        System.out.println("Radius of circle:");
        double radius = appScanner.nextDouble();

        double areaCircle = 3.1416 * radius * radius;

        System.out.println("The area of the circle is " + areaCircle);

    }

}
