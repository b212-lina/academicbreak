package com.zuitt.batch212;

import java.util.Arrays;

public class Array {

    public static void main(String[] args) {

        String[] names = {"Jenny", "Johann", "Jason"};

        System.out.println(Arrays.toString(names));

    }
}
