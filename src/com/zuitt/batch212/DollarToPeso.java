package com.zuitt.batch212;

import java.util.Scanner;

public class DollarToPeso {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        // Problem 4: Dollar to Peso Conversion

        System.out.println("Dollar to Peso Conversion");

        System.out.println("Dollar amount:");
        double dollar = appScanner.nextDouble();

        double peso = dollar * 56.16;

        System.out.println("Amount in peso: " + peso );

    }
}
