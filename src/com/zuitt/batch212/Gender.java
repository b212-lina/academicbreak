package com.zuitt.batch212;

import java.util.Scanner;

public class Gender {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // Problem 3: Gender
        System.out.println("m for male. f for female.");
        System.out.println("Gender:");
        char gender = scanner.next().charAt(0);
        String greeting;

        switch (gender) {

            case 'm':
                greeting = "Hello sir";
                break;

            case 'f':
                greeting = "Hello madam";
                break;

            default:
                greeting = "Unknown gender";
                break;

        }

        System.out.println(greeting);

    }

}
