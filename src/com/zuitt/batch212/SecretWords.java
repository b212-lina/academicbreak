package com.zuitt.batch212;

import java.util.Scanner;

public class SecretWords {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        // Problem 2 : Secret Words
        System.out.println("Enter a number to reveal secret words:");
        double number = appScanner.nextDouble();

        if (number == 143) {

            System.out.println("I love you!");

        }

        else {

            System.out.println("Wrong guess!!!");

        }

    }

}
