package com.zuitt.batch212;

import java.util.Scanner;

public class DayOfWeek {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // Problem 2: Day of the Week
        System.out.println("1-Monday, 2-Tuesday, 3-Wednesday, 4-Thursday, 5-Friday, 6-Saturday, 7-Sunday");
        System.out.println("Enter corresponding number of day:");
        byte dayNumber = scanner.nextByte();
        String day;

        switch (dayNumber) {

            case 1:
                day = "Monday";
                break;

            case 2:
                day = "Tuesday";
                break;

            case 3:
                day = "Wednesday";
                break;

            case 4:
                day = "Thursday";
                break;

            case 5:
                day = "Friday";
                break;

            case 6:
                day = "Saturday";
                break;

            case 7:
                day = "Sunday";
                break;

            default:
                day = "Invalid day. Try again.";
                break;

        }

        System.out.println("The day is " + day);

    }

}
