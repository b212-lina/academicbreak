package com.zuitt.batch212;

import java.util.Scanner;

public class PascalTriangle {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Input number of rows: ");
        int row = scanner.nextInt();
        int c = 1;


        for (int i = 0; i < row; i++) {

            for (int x = 1; x <= row-i; x++) {

                System.out.print(" ");

            }

            for(int y = 0; y <= i; y++) {

                if (y == 0 || i == 0) {

                    System.out.print(" " + c);

                } else {

                    c = c * (i - y + 1) / y;

                    System.out.print(" " + c);

                }

            }

            System.out.print("\n");

        }

    }

}
