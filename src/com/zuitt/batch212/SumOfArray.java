package com.zuitt.batch212;

import java.util.Arrays;
import java.util.Scanner;

public class SumOfArray {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] arr;
        int sum = 0;
        double average;
        arr = new int[5];
        System.out.println("Enter five numbers");
        System.out.println("Enter first number:");
        arr[0] = scanner.nextInt();

        System.out.println("Enter second number:");
        arr[1] = scanner.nextInt();

        System.out.println("Enter third number:");
        arr[2] = scanner.nextInt();

        System.out.println("Enter fourth number:");
        arr[3] = scanner.nextInt();

        System.out.println("Enter fifth number:");
        arr[4] = scanner.nextInt();

        for (int i = 0; i < 5; i ++) {

            sum = sum + arr[i];

        }

        average = sum / 5;

        System.out.println(Arrays.toString(arr));
        System.out.println("The sum of all numbers is " + sum + ".");
        System.out.println("The average of all numbers is " + average + ".");
    }
}
