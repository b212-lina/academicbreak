package com.zuitt.batch212;

import java.util.Scanner;

public class EmployeeSalary {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        // Problem 3: Employee's Salary

        System.out.println("Salary Calculator");

        System.out.println("Employee name:");
        String name = appScanner.nextLine().trim();

        System.out.println("Days of work:");
        double days = appScanner.nextDouble();

        System.out.println("Daily rate:");
        double rate = appScanner.nextDouble();

        double salary = days * rate;

        System.out.println(name + "'s salary is " + salary);

    }
}
