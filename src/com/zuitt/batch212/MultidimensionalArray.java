package com.zuitt.batch212;

import java.util.Arrays;

public class MultidimensionalArray {

    public static void main(String[] args) {

        String[][] music = {{"Adie", "Arthur Nery", "Niki"}, {"Turbo Mouse", "Eraserheads", "December Avenue", "UDD"}, {"Lany", "Lauv", "The Chainsmokers", "Paramore"}};

        System.out.println(Arrays.deepToString(music));

    }

}
