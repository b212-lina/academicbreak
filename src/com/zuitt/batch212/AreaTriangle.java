package com.zuitt.batch212;

import java.util.Scanner;

public class AreaTriangle {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        // Problem 2: Area of Triangle

        System.out.println("Area of Triangle Calculator");

        System.out.println("Base of triangle:");
        double base = appScanner.nextDouble();

        System.out.println("Height of triangle:");
        double height = appScanner.nextDouble();

        double areaTriangle = (base * height) / 2;

        System.out.println("The area of the triangle is " + areaTriangle);

    }
}
