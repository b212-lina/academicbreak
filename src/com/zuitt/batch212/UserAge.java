package com.zuitt.batch212;

import java.util.Scanner;

public class UserAge {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        // Problem 3: User's Age Checker
        System.out.println("Enter user's age:");
        byte age = appScanner.nextByte();

        if (age > 18 && age < 40) {

            System.out.println("The user is between the age of 18 and 40 yrs old.");

        } else if (age < 50 && age > 39) {

            System.out.println("The user is between the age of 40 and 50 yrs old.");

        } else if (age > 50) {

            System.out.println("The user is 50 yrs old and above.");

        } else {

            System.out.println("The user is 18 yrs old or below.");

        }

    }

}
