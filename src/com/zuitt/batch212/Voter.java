package com.zuitt.batch212;

import java.util.Scanner;

public class Voter {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        // Problem 1 : Voter Verifier
        System.out.println("User's age:");
        byte age = appScanner.nextByte();

        if (age > 17) {

            System.out.println("User is a voter.");

        }

        else {

            System.out.println("User is not a voter.");

        }

    }

}
