package com.zuitt.batch212;

import java.util.Scanner;

public class CivilStatus {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        // Problem 4: Civil Status Checker
        System.out.println("True or false");
        System.out.println("Are you married?");
        boolean married = appScanner.nextBoolean();

        if (married) {

            System.out.println("Is your husband/wife still alive?");
            boolean alive = appScanner.nextBoolean();

            if (alive) {

                System.out.println("Are you separated?");
                boolean separated = appScanner.nextBoolean();

                if (separated) {

                    System.out.println("Are you legally separated?");
                    boolean legallySeparated = appScanner.nextBoolean();

                    if (legallySeparated) {

                        System.out.println("The user is annulled.");

                    } else {

                        System.out.println("The user is separated.");
                    }

                } else {

                    System.out.println("The user is married.");
                }

            } else {

                System.out.println("The user is a widow.");
            }

        } else {

            System.out.println("The user is single.");
        }

    }

}
