package com.zuitt.batch212;

import java.util.Scanner;

public class YearLevel {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // Problem 1: Year Level
        System.out.println("Enter of year of entry:");
        int year = scanner.nextInt();
        String yearLevel;

        switch (year) {

            case 2022:
                yearLevel = "Freshman";
                break;

            case 2021:
                yearLevel = "Sophomore";
                break;

            case 2020:
                yearLevel = "Junior";
                break;

            case 2019:
                yearLevel = "Senior";
                break;

            default:
                yearLevel = "Unknown";
                break;

        }

        System.out.println("The student is a " + yearLevel);

    }

}
