package com.zuitt.batch212;

public class MultiplicationTable {

    public static void main(String[] args) {

        int n = 10;

        for (int i = 1; i <= n; i++) {

            for (int x = 1; x <= n; x++) {

                System.out.println(i + " x " + x + " = " + i*x);

            }

        }

    }

}
